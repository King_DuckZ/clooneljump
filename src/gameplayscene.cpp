/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gameplayscene.hpp"
#include "mover.hpp"
#include "drawable.hpp"
#include "drawableset.hpp"
#include "placeable.hpp"
#include <unordered_set>

namespace cloonel {
	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	GameplayScene::GameplayScene (SDLMain* parSdlMain) :
		GameBase(parSdlMain)
	{
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void GameplayScene::OnUpdate (float parDelta) {
		{
			std::unordered_set<Placeable*> notify;
			for (auto mover : m_movers) {
				mover->CopyPlaceables(notify);
			}
			for (auto placeable : notify) {
				assert(placeable);
				placeable->BeginMovement();
			}
		}
		for (auto mover : m_movers) {
			mover->Update(parDelta);
		}
		m_collider.RunCollisionTests(parDelta);
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void GameplayScene::OnRender() {
		std::vector<const Drawable*> drawables;
		for (auto drawableSet : m_drawableSets) {
			drawables.clear();
			drawableSet->CopyDrawables(drawables);
			for (auto drawable : drawables) {
				drawable->Draw();
			}
		}
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void GameplayScene::Destroy() noexcept {
		m_collider.Reset();
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void GameplayScene::OnPrepareDone() {
	}
} //namespace cloonel
