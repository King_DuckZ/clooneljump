/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "vector.hpp"
#include "line.hpp"
#include "geometry.hpp"
#include "compatibility.h"
#include <ciso646>
#include <algorithm>
#include <utility>

#ifndef id2B404E7CFDD3470680E572779CB819F6
#define id2B404E7CFDD3470680E572779CB819F6

namespace cloonel {
	template <typename T>
	int GetOrientation ( const Vector<T, 2>& parPointA, const Vector<T, 2>& parPointB, const Vector<T, 2>& parPointC ) a_pure;

	template <typename T>
	bool Intersection2D ( const Vector<T, 2>& parSegA1, const Vector<T, 2>& parSegA2, const Vector<T, 2>& parSegB1, const Vector<T, 2>& parSegB2 ) a_pure;

	template <typename T>
	std::pair<bool, Vector<T, 2>> Intersection ( const Line<T, 2>& parSegA, const Line<T, 2>& parSegB ) a_pure;
} //namespace cloonel

#include "geometry_2d.inl"

#endif
