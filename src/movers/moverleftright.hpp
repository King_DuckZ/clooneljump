/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef id340A16673C014E0BBFE8AC24D8FC1C77
#define id340A16673C014E0BBFE8AC24D8FC1C77

#include "moveroneshot.hpp"

namespace cloonel {
	class MoverLeftRight : public MoverOneShot {
	public:
		enum MovementDirectionType {
			MovementDirection_Left,
			MovementDirection_Right,
			MovementDirection_Still
		};

		MoverLeftRight ( float parMaxVelocity, float parMass, float parForce );
		virtual ~MoverLeftRight ( void ) noexcept = default;

		void SetMovement ( MovementDirectionType parDirection );

	private:
		virtual float2 GetOffset ( void ) const;
		virtual void ApplyMotion ( float parDelta );

		float m_velocity;
		float m_maxVelocity;
		float m_invMass;
		float m_force;
		float m_movementTarget;
	};
} //namespace cloonel

#endif
