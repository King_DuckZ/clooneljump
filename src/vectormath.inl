/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace cloonel {
	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	template <typename T, typename U, uint32_t S>
	typename std::common_type<T, U>::type  dot (const Vector<T, S>& parA, const Vector<U, S>& parB) {
		typename std::common_type<T, U>::type retVal(0);
		for (uint32_t z = 0; z < S; ++z) {
			retVal += parA[z] * parB[z];
		}
		return retVal;
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	template <typename T, typename U>
	Vector<typename std::common_type<T, U>::type, 3>  cross (const Vector<T, 3>& /*parA*/, const Vector<U, 3>& /*parB*/) {
		//Not implemented
		assert(false);
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	template <typename T, uint32_t S>
	T len (const Vector<T, S>& parVector) {
		return std::sqrt(len_sq<T, S>(parVector));
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	template <typename T, uint32_t S>
	T len_sq (const Vector<T, S>& parVector) {
		T retVal(0);
		for (uint32_t z = 0; z < S; ++z) {
			retVal += parVector[z] * parVector[z];
		}
		return retVal;
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	template <typename T, uint32_t S>
	Vector<T, S> normalized (const Vector<T, S>& parVector) {
		return parVector / len(parVector);
	}

} //namespace cloonel
