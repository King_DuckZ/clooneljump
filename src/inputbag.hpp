/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef idF4E67FC292A5480DA4305B806170F520
#define idF4E67FC292A5480DA4305B806170F520

#include "inputdevicetype.hpp"
#include <memory>
#include <ciso646>

namespace cloonel {
	struct Key;

	class InputBag {
	public:
		enum ActionStateType {
			ActionState_Released,
			ActionState_Pressed,
			ActionState_JustReleased,
			ActionState_JustPressed
		};

		InputBag ( void );
		~InputBag ( void ) noexcept;

		void AddAction ( int parAction, const Key& parKey, std::string&& parName );
		ActionStateType ActionState ( int parAction ) const;
		void Clear ( void );
		void NotifyKeyAction ( InputDeviceType parDev, int parScancode, bool parPressed );
		void KeyStateUpdateFinished ( void ) noexcept;
		void ClearState ( void );

	private:
		struct LocalData;

		const std::unique_ptr<LocalData> m_localdata;
	};

	bool IsPressed ( const InputBag& parInput, int parAction );
	inline bool IsReleased ( const InputBag& parInput, int parAction ) { return not IsPressed(parInput, parAction); }
} //namespace cloonel

#endif
