/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef idB56C30EE09454EA2864FA6D1607131D5
#define idB56C30EE09454EA2864FA6D1607131D5

#include "collisionbarset.hpp"
#include "drawableset.hpp"
#include "movers/mover.hpp"
#include <list>
#include <boost/circular_buffer.hpp>

namespace cloonel {
	class SDLMain;
	class Platform;
	class Texture;

	class PlatformSet : public CollisionBarSet, public DrawableSet {
	public:
		PlatformSet ( SDLMain* parSdl );
		PlatformSet ( const PlatformSet& ) = delete;
		~PlatformSet ( void ) noexcept;
		PlatformSet& operator= ( const PlatformSet& ) = delete;

		void Add ( const float2& parPos, const float2& parSize, Texture* parTexture );
		size_t CountFreePlatforms ( void ) const;
		void SendBeginMovement ( void ) noexcept;
		void SendAddOffset ( const float2& parOffset ) noexcept;

		size_t size ( void ) const { return m_platforms.size(); }
		bool empty ( void ) const { return m_platforms.empty(); }
		void clear ( void ) noexcept;
		const Platform& back ( void ) { return m_platforms.back(); }

	private:
		//Overrides
		virtual void CopyBars ( std::vector<const HorzCollisionBar*>& parOut ) const;
		virtual void CopyDrawables ( std::vector<const Drawable*>& parOut ) const;

		boost::circular_buffer<Platform> m_platforms;
		SDLMain* const m_sdlmain;
	};
} //namespace cloonel

#endif
