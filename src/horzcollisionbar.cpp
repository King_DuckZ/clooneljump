/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "horzcollisionbar.hpp"
#include "line_helpers.hpp"
#include "maths.hpp"
#include <cassert>
#include <utility>
#include <ciso646>

namespace cloonel {
	namespace {
		typedef HorzCollisionBar::Line2D Line2D;

		float calculateOverlappingTime ( float parDeltaT, float parAY, float parBY, float parDeltaA, float parDeltaB ) a_pure;
		void DoNothing ( const Line2D&, const float2& ) a_pure;
		std::pair<bool, Line2D> getOverlap ( float parDeltaT, const Line2D& parA, const Line2D& parB, const float2& parDeltaA, const float2& parDeltaB ) a_pure;

		///----------------------------------------------------------------------
		///Calculate the time t at which the two segments (which are assumed to
		///be horizontal) will lie on the same line. parDeltaT is the duration
		///of the time frame being considered, parAY and parBY are the starting
		///y position of the two segments, parDeltaA and parDeltaB are the
		///distance traveled by segment A and segment B respectively in the
		///reference time frame.
		///----------------------------------------------------------------------
		float calculateOverlappingTime (float parDeltaT, float parAY, float parBY, float parDeltaA, float parDeltaB) {
			//const float deltaDiff = std::max(parDeltaA, parDeltaB) - std::min(parDeltaA, parDeltaB);
			const float deltaDiff = std::abs(parDeltaA - parDeltaB);

			if (deltaDiff <= 0.00001f)
				return 0.0f;
			else
				return (parDeltaT * (parBY - parAY)) / (parDeltaA - parDeltaB);
		}

		///----------------------------------------------------------------------
		///----------------------------------------------------------------------
		std::pair<bool, Line2D> getOverlap (float parDeltaT, const Line2D& parA, const Line2D& parB, const float2& parDeltaA, const float2& parDeltaB) {
			assert(parDeltaT > 0.0f);
			const float overlapTime = calculateOverlappingTime(parDeltaT, parA.Start().y(), parB.Start().y(), parDeltaA.y(), parDeltaB.y());
			if (overlapTime < 0.0f or overlapTime > parDeltaT)
				return std::make_pair<bool, Line2D>(false, Line2D());

			assert(overlapTime <= parDeltaT);
			const auto midpointA(LerpRange(parA, parDeltaA, overlapTime / parDeltaT));
			const auto midpointB(LerpRange(parB, parDeltaB, overlapTime / parDeltaT));

			if (midpointA.Start().x() >= midpointB.End().x() or midpointB.Start().x() >= midpointA.End().x())
				return std::make_pair<bool, Line2D>(false, Line2D());

			const auto& retStart = (midpointA.Start().x() > midpointB.Start().x() ? midpointA.Start() : midpointB.Start());
			const auto& retEnd = (midpointA.End().x() < midpointB.End().x() ? midpointA.End() : midpointB.End());
			return std::make_pair(true, Line2D(retStart, float2(retEnd.x(), retStart.y())));
		}

		///----------------------------------------------------------------------
		///no-op
		///----------------------------------------------------------------------
		void DoNothing (const HorzCollisionBar::Line2D&, const float2&) {
		}
	} //unnamed namespace

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	HorzCollisionBar::HorzCollisionBar (const float2& parFrom, float parLength) :
		Placeable(parFrom),
		m_segment(parFrom, float2(1.0f, 0.0f), parLength),
		m_offset(0.0f),
		m_callback(&DoNothing)
	{
		assert(parLength != 0.0f);
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void HorzCollisionBar::SetCallback (CallbackType parCallback) {
		m_callback = parCallback;
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	float2 HorzCollisionBar::To() const {
		assert(m_segment.End().x() - m_segment.Start().x() != 0.0f);
		return this->GetPos() + float2(std::abs(m_segment.End().x() - m_segment.Start().x()), 0.0f);
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void HorzCollisionBar::InvokeCallback (const Line2D& parOverlap, const float2& parDirection) const {
		m_callback(parOverlap, parDirection);
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void HorzCollisionBar::AddOffset (const float2& parOffset) noexcept {
		Placeable::AddOffset(parOffset);
		m_segment += parOffset;
		m_offset += parOffset;
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void HorzCollisionBar::BeginMovement() {
		m_offset = float2(0.0f);
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	bool Collide (float parDeltaT, const HorzCollisionBar& parA, const HorzCollisionBar& parB, HorzCollisionBar::Line2D& parOut) {
		const auto overlap(getOverlap(parDeltaT, parA.m_segment, parB.m_segment, parA.GetOffset(), parB.GetOffset()));

		if (overlap.first) {
			parOut = overlap.second;
			return true;
		}
		else {
			return false;
		}
	}
} //namespace cloonel
