/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef idC54817CCCC0F454F931AE9082DFE9FDA
#define idC54817CCCC0F454F931AE9082DFE9FDA

#include <cstdint>

#define DEF_PHYSICSFS_BUFFERED true

namespace cloonel {
	class PhysicsFSWrapper {
	public:
		explicit PhysicsFSWrapper ( const char* parBasePath );
		~PhysicsFSWrapper ( void ) noexcept;

		void Append ( const char* parRelativePath, const char* parMountPoint );
	};

	class PhysicsFSFile {
	public:
		enum OpenMode {
			OpenMode_Read,
			OpenMode_Write,
			OpenMode_Append
		};

		PhysicsFSFile ( const char* parPath, OpenMode parMode, const char* parDescCategory, bool parBuffered = DEF_PHYSICSFS_BUFFERED );
		~PhysicsFSFile ( void ) noexcept;

		bool IsOpen ( void ) const noexcept;
		int64_t Read ( void* parBuff, uint32_t parSize, uint32_t parCount );
		int64_t Write ( void* parBuff, uint32_t parSize, uint32_t parCount );
		bool IsEof ( void ) const noexcept;
		int64_t Tell ( void ) const;
		int Seek ( uint64_t parPos );
		void Flush ( void ) noexcept;

	private:
		void* const m_handle;
	};
} // namespace cloonel
#endif
