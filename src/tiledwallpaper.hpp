/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef idDDB250A761E9458899C0687B7C8C5B7D
#define idDDB250A761E9458899C0687B7C8C5B7D

#include "drawable.hpp"
#include "sizenotifiable.hpp"
#include "drawableset.hpp"
#include <memory>
#include <vector>

namespace cloonel {
	class Texture;
	class SDLMain;

	class TiledWallpaper : public Drawable, public DrawableSet {
	public:
		TiledWallpaper ( const std::string&& parPath, SDLMain* parMain );
		virtual ~TiledWallpaper ( void ) noexcept;

		void Reload ( void );
		void Destroy ( void ) noexcept;

		//Overrides
		virtual void Draw ( void ) const;
		virtual void CopyDrawables ( std::vector<const Drawable*>& parOut ) const { parOut.push_back(this); }

	private:
		class TileCountNotifiable : public SizeNotifiable<regbehaviours::AutoRegister> {
			typedef SizeNotifiable<regbehaviours::AutoRegister> BaseClass;
		public:
			TileCountNotifiable ( SDLMain* parMain, const ushort2& parTileSize );
			virtual ~TileCountNotifiable ( void ) noexcept = default;

			virtual void NotifyResChanged ( const SizeRatio& parSize );
			const float2& tileCount ( void ) const noexcept { return m_tileCount; }
			const float2& tileSize ( void ) const noexcept { return m_tileSize; }

		private:
			float2 m_tileCount;
			const float2 m_tileSize;
		};

		TileCountNotifiable m_tileCount;
		const std::unique_ptr<Texture> m_tile;
	};
} //namespace cloonel

#endif
