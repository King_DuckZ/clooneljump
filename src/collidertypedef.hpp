/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef id3DF7C722C6AE4978874605F38F26E350
#define id3DF7C722C6AE4978874605F38F26E350

#include <functional>

namespace cloonel {
	class HorzCollisionBar;
	typedef std::function<void(HorzCollisionBar*)> ColliderRegisterFunc;
	typedef std::function<void(HorzCollisionBar*)> ColliderUnregisterFunc;
} //namespace cloonel

#endif
