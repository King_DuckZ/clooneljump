/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef id1DF84BC48C0547D69F79499E3A25BFC5
#define id1DF84BC48C0547D69F79499E3A25BFC5

#include <vector>
#include <cassert>
#include "gamebase.hpp"
#include "collider.hpp"

namespace cloonel {
	class Mover;
	class DrawableSet;

	class GameplayScene : public GameBase {
	public:
		explicit GameplayScene ( SDLMain* parSdlMain );
		virtual ~GameplayScene ( void ) noexcept = default;

		void AddMover ( Mover* parMover ) { assert(parMover); m_movers.push_back(parMover); }
		void AddDrawableSet ( const DrawableSet* parSet ) { assert(parSet); m_drawableSets.push_back(parSet); }

		virtual void Destroy ( void ) noexcept;

	protected:
		Collider* GetCollider ( void ) { return &m_collider; }

	private:
		virtual void OnRender ( void );
		virtual void OnUpdate ( float parDelta );
		virtual void OnPrepareDone ( void );

		Collider m_collider;
		std::vector<Mover*> m_movers;
		std::vector<const DrawableSet*> m_drawableSets;
	};
} //namespace cloonel

#endif
