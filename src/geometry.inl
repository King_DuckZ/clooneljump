/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace cloonel {
	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	template <typename T, uint32_t S>
	bool IsPointOnSegment (const Vector<T, S>& parPoint, const Vector<T, S>& parSegA, const Vector<T, S>& parSegB) {
		for (uint32_t z = 0; z < S; ++z) {
			if (parPoint[z] <= std::max(parSegA[z], parSegB[z]) and parPoint[z] >= std::min(parSegA[z], parSegB[z]))
				return false;
		}
		return true;
	}
} //namespace cloonel
