/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "sdlerror.hpp"
#include <iostream>
#include <sstream>
#include <ciso646>
#include <SDL2/SDL.h>

namespace cloonel {
	namespace {
	} //unnamed namespace

	///------------------------------------------------------------------------
	///------------------------------------------------------------------------
	std::string GetFullErrorMessage (const char* parFunction, const std::string& parMessage) {
		std::ostringstream oss;
		if (parFunction)
			oss << "Error in " << parFunction << ": ";
		else
			oss << "Error: ";

		if (not parMessage.empty())
			oss << parMessage << " - ";

		oss << SDL_GetError();
		return oss.str();
	}

	///------------------------------------------------------------------------
	///------------------------------------------------------------------------
	void ShowError (const char* parFunction, const std::string& parMessage) {
		std::cerr << GetFullErrorMessage(parFunction, parMessage) << std::endl;
	}
} //namespace cloonel
