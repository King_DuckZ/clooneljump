/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef id703E4B8DFFF747DFA97864384B87E9C1
#define id703E4B8DFFF747DFA97864384B87E9C1

#include "vector.hpp"
#include "mover.hpp"

namespace cloonel {
	class Placeable {
	public:
		float2 GetPos ( void ) const noexcept;
		virtual void AddOffset ( const float2& parOffset ) noexcept;
		virtual void SetAbsolutePosition ( const float2& parPos ) noexcept;
		virtual void OnRegister ( Mover& parMover, Mover::PlaceableTicketType parParentTicket );
		virtual void BeginMovement ( void );

	protected:
		explicit Placeable ( float2 parPos );
		virtual ~Placeable ( void ) noexcept = default;

	private:
		float2 m_pos;
	};

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	inline void Placeable::AddOffset (const float2& parOffset) noexcept {
		m_pos += parOffset;
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	inline float2 Placeable::GetPos() const noexcept {
		return m_pos;
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	inline void Placeable::SetAbsolutePosition (const float2& parPos) noexcept {
		m_pos = parPos;
	}
} //namespace cloonel

#endif
