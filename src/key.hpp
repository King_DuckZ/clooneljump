/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef id8F6145D6CFBA40338C5804DEC032CE16
#define id8F6145D6CFBA40338C5804DEC032CE16

#include "inputdevicetype.hpp"
#include <ciso646>

namespace cloonel {
	struct Key {
		Key ( void ) = default;
		Key ( InputDeviceType parDev, int parScancode, const char parLabel[4] ) :
			srcdevice(parDev),
			scancode(parScancode)
		{
			label[0] = parLabel[0];
			label[1] = parLabel[1];
			label[2] = parLabel[2];
			label[3] = parLabel[3];
		}
		Key ( InputDeviceType parDev, int parScancode ) :
			srcdevice(parDev),
			scancode(parScancode),
			label {0, 0, 0, 0}
		{
		}
		~Key ( void ) noexcept = default;

		bool operator== ( const Key& parOther ) const {
			return srcdevice == parOther.srcdevice and scancode == parOther.scancode;
		}
		bool operator!= ( const Key& parOther ) const {
			return not this->operator==(parOther);
		}
		bool operator< ( const Key& parOther ) const {
			return (srcdevice == parOther.srcdevice and scancode < parOther.scancode) or (srcdevice < parOther.srcdevice);
		}

		InputDeviceType srcdevice;
		int scancode;
		char label[4];
	};
} //namespace cloonel

#endif
