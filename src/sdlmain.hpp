/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef id8E7A054DAC9040B887F2620EFD229EE8
#define id8E7A054DAC9040B887F2620EFD229EE8

#include "vector.hpp"
#include <memory>
#include <string>

struct SDL_Renderer;

namespace cloonel {
	class SizeNotifiableBase;

	class SDLMain {
	public:
		SDLMain ( const char* parGameName, ushort2 parRes, ushort2 parReferenceRes );
		~SDLMain ( void ) noexcept;

		void Init ( void );
		SDL_Renderer* GetRenderer ( void );
		ushort2 WidthHeight ( void ) const noexcept;

		void SetResolution ( ushort2 parRes );
		size_t RegisterForResChange ( SizeNotifiableBase* parNotif );
		void UnregisterForResChange ( size_t parID ) noexcept;
		void SwapRegisteredForResChange ( size_t parID, SizeNotifiableBase* parNotif );
		const std::string& GetRendererName ( void ) const { return m_rendererName; }
		std::string GetVideoDriverName ( void ) const;

	private:
		struct LocalData;

		void InitSDL ( LocalData& parData );
		void ClearIFN ( LocalData& parData ) noexcept;

		const std::string m_gameName;
		std::string m_rendererName;
		std::unique_ptr<LocalData> m_localData;
	};
} //namespace cloonel

#endif
