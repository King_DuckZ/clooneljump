#CloonelJump
##Description
CloonelJump is an open source clone of the more popular Doodle Jump.
In this SDL/C++ game the player has to continously jump over platforms and
climb to the top in a never-ending scenario. Platforms that scroll down past
the bottom of the screen will disappear, meaning that missing a jump and falling
to the bottom of the screen leads to the character's death and player's game
over.
Climb up as much as you can to make the highest record.

##Requirements
###Platforms
CloonelJump is being developed on 64 bit Linux. At the moment that's the only supported platforms, but in the future it will be ported to SailfishOS and Android.
###Dependencies
At the moment, CloonelJump needs the following libraries to run:

+ libPNG
+ SDL2
+ Boost

Additionally, to build it you will need cmake.

##Building
Run cmake to generate the project for your platform. For debug builds using ninja I run:

    cd ~
    mkdir cloonel_build
    cd cloonel_build
    cmake -DCMAKE_BUILD_TYPE=Debug -GNinja <path_to_the_source>
    ninja

Replace Debug with Release for a Release build, and omit the -G parameter to use the default generator on your platform, if you don't use ninja.

##Contact
If you want to talk to me, you can find me on IRC Freenode most of the time as King_DuckZ
